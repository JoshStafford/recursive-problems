def is_power(a, b):

    if (a%b == 0):

        c = a / b

        pow = 1

        while(pow < c):

           pow *= b

        if(pow == c):
            return True
        else:
            return False

    # Does not need to be recursive at all



def sum_digits(digits, total=0):
    
    digits = str(digits)
    digits = [x for x in digits]

    total += int(digits[0])
    digits.pop(0)

    if(len(digits) == 0):
        return total
    else:
        digits = ''.join(digits)
        return sum_digits(int(digits), total)
    


def iselfish(word, found=[]):
    print(found)
    elf = ['e', 'l', 'f']
    
    letters = [x for x in word]

    letter = letters.pop(0)
    print(letter)

    if(letter in elf and letter not in found):
        found.append(letter)
    
    if('e' in found and 'l' in found and 'f' in found):
        return True
    elif(len(letters) == 0):
        return False
    else:
        return iselfish(''.join(letters), found)



def flatten(mlist, flat=[]):

    item = mlist.pop(0)

    if(item is list):
        flat.append(flatten(mlist, flat))
    else:
        if(len(mlist) == 0):
            return flat



